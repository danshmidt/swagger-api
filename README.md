{
  "swagger": "2.0",
  "info": {
    "description": "Pilot is the main entity in the platform. It is typically created and owned by a company of type \"client\", and companies of type \"vendor\" could then join and participate in it.",
    "version": "2.1",
    "title": "Pilot  API"
  },
  "host": "proov.io",
  "basePath": "/api",
  "tags": [
    {
      "name": "Get Pilot",
      "description": "Support unsubscribed & subscribed US users with different field sets"
    },
    {
      "name": "Patch Pilot",
      "description": "Update existing pilot"
    },
    {
      "name": "Post Pilot",
      "description": "Create new pilot"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/pilot/{pilot_id}": {
      "get": {
        "tags": [
          "Get Pilot"
        ],
        "description": "Get pilot By ID",
        "operationId": "getPilot",
        "parameters": [
          {
            "in": "path",
            "name": "pilot_id",
            "type": "string",
            "description": "Pilot ID",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "ok - This example for unsubscribed user. For a subscribed user, the entire set of pilot field return.",
            "schema": {
              "example": {
                "id": "2c9f91cc581b679301582b31b3da00ef",
                "name": "Adaptive Security Technology",
                "company": {
                  "id": null,
                  "name": null,
                  "type": null,
                  "profile_image": null,
                  "about": null
                },
                "status": "PENDING",
                "description": "Amapap ukpib jehimpug veswo wekeche obazub",
                "num_vendors": 2,
                "joinable": "pending_subscr",
                "start_time": "2016-02-15T07:55:54.666Z",
                "end_time": "2018-04-31T07:55:54.666Z"
              }
            }
          }
        }
      },
      "patch": {
        "tags": [
          "Patch Pilot"
        ],
        "operationId": "patchPilot",
        "parameters": [
          {
            "in": "path",
            "name": "pilot_id",
            "type": "string",
            "description": "Pilot ID",
            "required": true
          },
          {
            "in": "body",
            "name": "pilot",
            "schema": {
              "type": "object",
              "example": {
                "name": "An updated pilot name",
                "country": "il",
                "budget": "up_to_100K",
                "tech_contact_phone": null,
                "country_code": "AF",
                "phone_number": "8610355"
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "example": {
                "name": "An updated pilot name",
                "country": "il",
                "budget": "up_to_100K",
                "tech_contact_phone": null,
                "country_code": "AF",
                "phone_number": "8610355"
              }
            }
          },
          "500": {
            "description": "payment failed",
            "schema": {
              "example": {
                "error": {
                  "type": "insufficient funds",
                  "code": "insufficient_funds",
                  "message": "insufficient funds"
                }
              }
            }
          }
        }
      }
    },
    "/pilot": {
      "post": {
        "tags": [
          "Post Pilot"
        ],
        "description": "Create new pilot",
        "operationId": "createPilot",
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Pilot"
              }
            }
          }
        }
      }
    },
    "/pilot/{pilot_id}/complete": {
      "post": {
        "parameters": [
          {
            "in": "path",
            "name": "pilot_id",
            "type": "string",
            "description": "Pilot ID",
            "required": true
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Pilot"
              }
            }
          }
        }
      }
    },
    "/pilot/{pilot_id}/analytics": {
      "get": {
        "parameters": [
          {
            "in": "path",
            "required": true,
            "name": "pilot_id",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "ok"
          }
        }
      }
    },
    "/pilot/{pilot_id}/checkout": {
      "post": {
        "parameters": [
          {
            "name": "pilot_id",
            "in": "path",
            "type": "string",
            "description": "Pilot ID",
            "required": true
          },
          {
            "name": "stripe_token",
            "in": "body",
            "schema": {
              "type": "object",
              "description": "stripe charge token",
              "example": {
                "stripe_token": "424242424242424242"
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Pilot"
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Pilot": {
      "type": "object",
      "required": [
        "id",
        "company",
        "user_id",
        "created_at",
        "update_at",
        "name",
        "status",
        "is_public",
        "description",
        "snippet",
        "category",
        "country",
        "industry",
        "tags",
        "region_policy",
        "approval_policy",
        "start_time",
        "end_time",
        "last_join_time",
        "subscr_end_time",
        "max_vendors",
        "num_vendors",
        "nda",
        "goals",
        "target_vendors",
        "budget",
        "eval_criteria",
        "approval_policy_desc",
        "scenarios",
        "instructions",
        "technology",
        "tech_contact_name",
        "tech_contact_email",
        "tech_contact_phone",
        "environment",
        "dataset",
        "joinable",
        "vendors",
        "requests",
        "invitations",
        "completed",
        "related"
      ],
      "properties": {
        "id": {
          "description": "Pilot ID",
          "type": "string"
        },
        "company": {
          "description": "Company owns the pilot",
          "type": "object"
        },
        "user_id": {
          "description": "ID of the user that created the pilot",
          "type": "integer"
        },
        "created_at": {
          "description": "Timestamp for when the pilot was created",
          "type": "string",
          "format": "date-time"
        },
        "update_at": {
          "description": "Timestamp for when the pilot was updated",
          "type": "string",
          "format": "data-time"
        },
        "name": {
          "description": "Name of the pilot",
          "type": "string"
        },
        "status": {
          "description": "Pilot status",
          "type": "string",
          "enum": [
            "active",
            "completed",
            "future"
          ]
        },
        "is_public": {
          "description": "Whether the pilot be visible in marketplace",
          "default": true,
          "type": "boolean"
        },
        "description": {
          "description": "Pilot description",
          "type": "string"
        },
        "snippet": {
          "description": "A snippet of the pilot's description. max 200 charts",
          "type": "string"
        },
        "category": {
          "description": "Pilot category",
          "type": "array"
        },
        "country": {
          "description": "Pilot category",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "industry": {
          "description": "Pilot industrey",
          "type": "array"
        },
        "tags": {
          "description": "An array of tags",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "region_policy": {
          "description": "List the countries from where SU can join this pilot.",
          "default": "null for all countries",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "approval_policy": {
          "description": "The method of pilot approval",
          "default": "open",
          "type": "string",
          "enum": [
            "open",
            "manual"
          ]
        },
        "start_time": {
          "description": "Pilot start time",
          "type": "string",
          "format": "date-time"
        },
        "end_time": {
          "description": "Pilot end time",
          "type": "string",
          "format": "date-time"
        },
        "last_join_time": {
          "description": "Final date to join the pilot",
          "type": "string",
          "format": "date-time"
        },
        "subscr_end_time": {
          "description": "User subscription expiration time",
          "type": "string",
          "format": "date-time"
        },
        "max_vendors": {
          "description": "Pilot joing cap",
          "type": "integer"
        },
        "num_vendors": {
          "description": "Accepted participants",
          "type": "integer"
        },
        "nda": {
          "description": "NDA file",
          "type": "object"
        },
        "goals": {
          "type": "string"
        },
        "target_vendors": {
          "type": "string"
        },
        "budget": {
          "type": "string",
          "enum": [
            "up_to_10k",
            "up_to_25k",
            "up_to_50k",
            "up_to_100k",
            "up_to_250k",
            "more_than_250k"
          ]
        },
        "eval_criteria": {
          "description": "A collection of evaluation criterions, which will be used by the pilot owner to rate the vendors",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "approval_policy_desc": {
          "description": "owner approval criteria",
          "type": "string"
        },
        "scenarios": {
          "type": "string",
          "default": "what is it?"
        },
        "instructions": {
          "description": "Pilot owner directions on pilot outcome",
          "type": "string"
        },
        "technology": {
          "description": "Pilot technology utilization",
          "type": "string",
          "enum": [
            "window",
            "linux",
            "database",
            "api"
          ]
        },
        "tech_contact_name": {
          "type": "string"
        },
        "tech_contact_email": {
          "type": "string"
        },
        "tech_contact_phone": {
          "type": "object",
          "example": {
            "label": "dsds",
            "country": "",
            "phone_number": ""
          }
        },
        "environment": {
          "description": "Channels",
          "type": "array",
          "items": {
            "type": "object"
          }
        },
        "dataset": {
          "description": "User owned datasets",
          "type": "array",
          "items": {
            "type": "object"
          }
        },
        "joinable": {
          "type": "string",
          "enum": [
            "open",
            "pending_approval",
            "pending_subscr",
            "missing_info",
            "subscr_expired",
            "subscribed",
            "finished",
            "denied",
            "max_pilots",
            "invalid_region",
            "join_date_due",
            "max_vendors"
          ],
          "description": "User owned datasets"
        },
        "vendors": {
          "description": "Collection of participating vendors, which are vendors that were approved by the pilot owner and completed all the necessary steps to become participants",
          "type": "array"
        },
        "requests": {
          "description": "Collection of requests to join the pilot",
          "type": "array"
        },
        "request_id": {
          "description": "Vendor Join Request ID (required to complete join checkout)",
          "type": "string"
        },
        "invitations": {
          "description": "Collection of invitations to join the pilot",
          "type": "array"
        },
        "completed": {
          "description": "Collection of completion reports submitted by participants",
          "type": "array"
        },
        "related": {
          "description": "A collection of related or similar pilots",
          "type": "array"
        }
      }
    }
  }
}
